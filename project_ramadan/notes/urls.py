from .views import CreateNotesAPI, GetNotesAPI, UpdateNotesAPI
from django.urls import path

urlpatterns = [
    path("", CreateNotesAPI.as_view(), name="create-notes"),
    path("<int:noteId>", UpdateNotesAPI.as_view(), name="update-notes"),
    path("list/", GetNotesAPI.as_view(), name="list-notes"),
]
