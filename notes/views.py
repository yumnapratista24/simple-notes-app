from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.views import TokenObtainPairView

from .models import Notes
from .serializers import (
    CreateNotesSerializer,
    GetNotesSerializer,
    UpdateNotesSerializer,
)

# Create your views here.


class CreateNotesAPI(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CreateNotesSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data={
                "userId": request.user.id,
                "title": request.data["title"],
                "note": request.data["note"],
            }
        )

        serializer.is_valid(raise_exception=True)
        note = serializer.save()

        return Response({"message": "success"})


class UpdateNotesAPI(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UpdateNotesSerializer

    def update(self, request, *args, **kwargs):
        print(request.data)
        noteId = self.kwargs["noteId"]

        note = Notes.objects.get(id=noteId)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        note = self.buildUpdatedNote(note, serializer.data)
        note.save()

        return Response({"message": "success"})

    def buildUpdatedNote(self, note, serializedData):
        note.title = serializedData["title"]
        note.note = serializedData["note"]

        return note


class GetNotesAPI(generics.ListAPIView):
    serializer_class = GetNotesSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        return Notes.objects.filter(user=user).order_by("-created_at")
