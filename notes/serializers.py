from .models import Notes
from rest_framework import serializers

# Create Notes Serializer


class CreateNotesSerializer(serializers.Serializer):
    title = serializers.CharField()
    note = serializers.CharField()
    userId = serializers.IntegerField()

    def create(self, data):
        userId = data["userId"]
        title = data["title"]
        note = data["note"]

        note = Notes.objects.create(
            user_id=userId,
            title=title,
            note=note,
        )

        return note


class UpdateNotesSerializer(serializers.Serializer):
    title = serializers.CharField()
    note = serializers.CharField()


class GetNotesSerializer(serializers.Serializer):
    title = serializers.CharField()
    note = serializers.CharField()
    noteId = serializers.IntegerField(source="id")
    createdAt = serializers.CharField(source="created_at")
